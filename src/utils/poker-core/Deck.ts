import { Poke } from './Poker'
export class Deck {
  cards: Poke[] = []
  counter: number = 0

  constructor() {
    this.reset()
  }

  reset(): void {
    this.cards = []
    for (let color = 0; color < 4; color++) {
      for (let value = 2; value < 15; value++) {
        this.cards.push(new Poke(color, value))
      }
    }

    this.shuffle()

    this.counter = 0
  }

  shuffle(): void {
    for (let i = this.cards.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1))
      const tmp = this.cards[i]
      this.cards[i] = this.cards[j]
      this.cards[j] = tmp
    }
  }

  deal(): Poke {
    return this.cards[this.counter++]
  }
}
