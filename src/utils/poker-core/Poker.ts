export class Poke {
  private value: number
  private color: number

  constructor(color: number, value: number) {
    this.value = value
    this.color = color
  }

  public static FromByte(byteValue: number): Poke {
    const color = byteValue >> 4
    const value = Math.floor(byteValue) % 16
    return new Poke(color, value)
  }

  public getValue(): number {
    return this.value
  }

  public getColor(): number {
    return this.color
  }

  public toByte(): number {
    return (this.color << 4) | this.value
  }

  public toString(): string {
    return PokeColorName[this.color] + PokeValueName[this.value]
  }

  public isJoker(): boolean {
    return this.color === PokeColor.Joker && this.value === 8
  }

  public static isJoker(value: number): boolean {
    const color = value >> 4
    const v = value % 16
    return color === PokeJoker.color && v === PokeJoker.value
  }

  public static Compare(a: Poke, b: Poke): number {
    if (a.getValue() === b.getValue()) {
      return 1
    } else if (a.getValue() > b.getValue()) {
      return -1
    } else {
      return 0
    }
  }
}

export const PokeValue: {
  [key: string]: number
} = {
  Poke_A: 14,
  Poke_K: 13,
  Poke_Q: 12,
  Poke_J: 11,
  Poke_10: 10,
  Poke_9: 9,
  Poke_8: 8,
  Poke_7: 7,
  Poke_6: 6,
  Poke_5: 5,
  Poke_4: 4,
  Poke_3: 3,
  Poke_2: 2,
}
export const PokeValueName: {
  [key: number]: string
} = {
  14: 'A',
  13: 'K',
  12: 'Q',
  11: 'J',
  10: '10',
  9: '9',
  8: '8',
  7: '7',
  6: '6',
  5: '5',
  4: '4',
  3: '3',
  2: '2',
}

export const PokeValues = [
  PokeValue.Poke_2,
  PokeValue.Poke_3,
  PokeValue.Poke_4,
  PokeValue.Poke_5,
  PokeValue.Poke_6,
  PokeValue.Poke_7,
  PokeValue.Poke_8,
  PokeValue.Poke_9,
  PokeValue.Poke_10,
  PokeValue.Poke_J,
  PokeValue.Poke_Q,
  PokeValue.Poke_K,
  PokeValue.Poke_A,
]

export const PokeColor: {
  [key: string]: number
} = {
  Poke_Diamond: 0, //方塊
  Poke_Spade: 1, //梅花
  Poke_Heart: 2, //紅心
  Poke_Club: 3, //黑桃
  Poke_Joker: 5, //小王
}

export const PokeColorName: {
  [key: number]: string
} = {
  0: '♦', //方塊
  1: '♣', //梅花
  2: '♥', //紅心
  3: '♠', //黑桃
  5: '鬼', //小王
}

export const PokeColors = [
  PokeColor.Poke_Diamond,
  PokeColor.Poke_Spade,
  PokeColor.Poke_Heart,
  PokeColor.Poke_Club,
]

export const PokeJoker = new Poke(PokeColor.Joker, 8)
