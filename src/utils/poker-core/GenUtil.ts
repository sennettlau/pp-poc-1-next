import { Poke, PokeColor } from './Poker'

export class GenUtil {
  public static toString = (k: number): string => {
    const pokes = this.toArrayFromLong(k)
    let ret = ''
    for (const poke of pokes) {
      ret += poke.toString()
    }
    return ret
  }

  public static toArrayFromLong = (long: number): Poke[] => {
    const pokes: Poke[] = []
    if (long > 1000000000000) {
      pokes.push(Poke.FromByte((long % 100000000000000) / 1000000000000))
    }
    if (long > 10000000000) {
      pokes.push(Poke.FromByte((long % 1000000000000) / 10000000000))
    }
    if (long > 100000000) {
      pokes.push(Poke.FromByte((long % 10000000000) / 100000000))
    }
    if (long > 1000000) {
      pokes.push(Poke.FromByte((long % 100000000) / 1000000))
    }
    if (long > 10000) {
      pokes.push(Poke.FromByte((long % 1000000) / 10000))
    }
    if (long > 100) {
      pokes.push(Poke.FromByte((long % 10000) / 100))
    }
    if (long > 1) {
      pokes.push(Poke.FromByte((long % 100) / 1))
    }
    return pokes
  }

  public static genCardBind = (tmp: number[]): number => {
    let ret = 0
    for (let i = 0; i < tmp.length; i++) {
      ret = ret * 100 + tmp[i]
    }
    return ret
  }

  public static removeColor = (long: number): number => {
    let cs: number[] = []
    if (long > 1000000000000) {
      cs.push((long % 100000000000000) / 1000000000000)
    }
    if (long > 10000000000) {
      cs.push((long % 1000000000000) / 10000000000)
    }
    if (long > 100000000) {
      cs.push((long % 10000000000) / 100000000)
    }
    if (long > 1000000) {
      cs.push((long % 100000000) / 1000000)
    }
    if (long > 10000) {
      cs.push((long % 1000000) / 10000)
    }
    if (long > 100) {
      cs.push((long % 10000) / 100)
    }
    if (long > 1) {
      cs.push((long % 100) / 1)
    }

    for (let i = 0; i < cs.length; i++) {
      if (!Poke.isJoker(cs[i])) {
        cs[i] = (PokeColor.Poke_Diamond << 4) | cs[i] % 16
      }
    }
    cs = cs.sort((a, b) => a - b)
    let ret = 0
    for (let i = 0; i < cs.length; i++) {
      ret = ret * 100 + cs[i]
    }
    return ret
  }

  public static changeColor = (long: number) => {
    let cs: number[] = []
    if (long > 1000000000000) {
      cs.push((long % 100000000000000) / 1000000000000)
    }
    if (long > 10000000000) {
      cs.push((long % 1000000000000) / 10000000000)
    }
    if (long > 100000000) {
      cs.push((long % 10000000000) / 100000000)
    }
    if (long > 1000000) {
      cs.push((long % 100000000) / 1000000)
    }
    if (long > 10000) {
      cs.push((long % 1000000) / 10000)
    }
    if (long > 100) {
      cs.push((long % 10000) / 100)
    }
    if (long > 1) {
      cs.push((long % 100) / 1)
    }
    const color = [0, 0, 0, 0]
    for (let i = 0; i < cs.length; i++) {
      if (!Poke.isJoker(cs[i])) {
        color[cs[i] >> 4]++
      }
    }
    let maxColor = 0
    let maxColorNum = 0

    for (let i = 0; i < color.length; i++) {
      if (color[i] > maxColorNum) {
        maxColorNum = color[i]
        maxColor = i
      }
    }

    for (let i = 0; i < cs.length; i++) {
      if (!Poke.isJoker(cs[i])) {
        if (cs[i] >> 4 === maxColor) {
          cs[i] = (PokeColor.Poke_Club << 4) | cs[i] % 16
        } else {
          cs[i] = (PokeColor.Poke_Diamond << 4) | cs[i] % 16
        }
      }
    }
    cs = cs.sort((a, b) => a - b)
    return GenUtil.genCardBind(cs)
  }
}
