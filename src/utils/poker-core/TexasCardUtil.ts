import { Poke } from './Poker'

export class TexasCardUtil {
  public static TEXAS_CARD_TYPE_GAOPAI = 1 //高牌
  public static TEXAS_CARD_TYPE_DUIZI = 2 //對子
  public static TEXAS_CARD_TYPE_LIANGDUI = 3 //兩對
  public static TEXAS_CARD_TYPE_SANTIAO = 4 //三條
  public static TEXAS_CARD_TYPE_SHUNZI = 5 //順子
  public static TEXAS_CARD_TYPE_TONGHUA = 6 //同花
  public static TEXAS_CARD_TYPE_HULU = 7 //葫蘆
  public static TEXAS_CARD_TYPE_TIEZHI = 8 //四條
  public static TEXAS_CARD_TYPE_TONGHUASHUN = 9 //同花順
  public static TEXAS_CARD_TYPE_KINGTONGHUASHUN = 10 //皇家同花順

  public static getCardTypeByUnsortedPokes = (cards: Poke[]): number => {
    cards.sort(Poke.Compare)
    return TexasCardUtil.getCardType(cards)
  }
  //cards里的牌必须是排好序的 从大到小
  private static getCardType(cards: Poke[]): number {
    if (cards.length !== 5) {
      return 0
    }
    let sameColor = true
    let lineCard = true
    const firstColor = cards[0].getColor()
    const firstValue = cards[0].getValue()

    //牌型分析
    for (let i = 1; i < 5; ++i) {
      if (cards[i].getColor() !== firstColor) {
        sameColor = false
      }
      if (cards[i].getValue() + i !== firstValue) {
        lineCard = false
      }
      if (!sameColor && !lineCard) {
        break
      }
    }
    //最小顺子
    if (lineCard === false && firstValue === 14) {
      let value = 1
      let i = 1
      for (; i < 5; ++i) {
        value = cards[i].getValue()
        if (value + i + 8 !== firstValue) {
          break
        }
      }
      if (i === 5) {
        lineCard = true
      }
    }
    //皇家同花顺
    if (sameColor && lineCard && cards[1].getValue() === 13) {
      return TexasCardUtil.TEXAS_CARD_TYPE_KINGTONGHUASHUN
    }

    //同花顺
    if (sameColor && lineCard) {
      return TexasCardUtil.TEXAS_CARD_TYPE_TONGHUASHUN
    }

    //順子
    if (!sameColor && lineCard) {
      return TexasCardUtil.TEXAS_CARD_TYPE_SHUNZI
    }

    //同花
    if (sameColor && !lineCard) {
      return TexasCardUtil.TEXAS_CARD_TYPE_TONGHUA
    }

    //四條
    const analyseCards = AnalysePokeResult.analyseCards(cards)
    if (analyseCards.fourCount === 1) {
      return TexasCardUtil.TEXAS_CARD_TYPE_TIEZHI
    }
    if (analyseCards.threeCount === 1 && analyseCards.twoCount === 1) {
      return TexasCardUtil.TEXAS_CARD_TYPE_HULU
    }
    if (analyseCards.threeCount === 1) {
      return TexasCardUtil.TEXAS_CARD_TYPE_SANTIAO
    }
    if (analyseCards.twoCount === 2) {
      return TexasCardUtil.TEXAS_CARD_TYPE_LIANGDUI
    }
    if (analyseCards.twoCount === 1) {
      return TexasCardUtil.TEXAS_CARD_TYPE_DUIZI
    }
    return TexasCardUtil.TEXAS_CARD_TYPE_GAOPAI
  }

  public static getCardTypeName = (cardType: number): string => {
    switch (cardType) {
      case TexasCardUtil.TEXAS_CARD_TYPE_GAOPAI:
        return '高牌'
      case TexasCardUtil.TEXAS_CARD_TYPE_DUIZI:
        return '對子'
      case TexasCardUtil.TEXAS_CARD_TYPE_LIANGDUI:
        return '兩對'
      case TexasCardUtil.TEXAS_CARD_TYPE_SANTIAO:
        return '三條'
      case TexasCardUtil.TEXAS_CARD_TYPE_SHUNZI:
        return '順子'
      case TexasCardUtil.TEXAS_CARD_TYPE_TONGHUA:
        return '同花'
      case TexasCardUtil.TEXAS_CARD_TYPE_HULU:
        return '葫蘆'
      case TexasCardUtil.TEXAS_CARD_TYPE_TIEZHI:
        return '四條'
      case TexasCardUtil.TEXAS_CARD_TYPE_TONGHUASHUN:
        return '同花順'
      case TexasCardUtil.TEXAS_CARD_TYPE_KINGTONGHUASHUN:
        return '皇家同花順'
      default:
        return '未知'
    }
  }

  public static fiveFromCards = (cards: Poke[]): Poke[] => {
    let pickedCards: Poke[] = []
    const targetCards = [...cards].sort(Poke.Compare)
    if (targetCards.length <= 5) {
      return targetCards
    }
    let cardType = 0
    for (let i = 0; i < 3; ++i) {
      for (let j = i + 1; j < 4; ++j) {
        for (let k = j + 1; k < 5; ++k) {
          for (let l = k + 1; l < targetCards.length - 1; ++l) {
            for (let m = l + 1; m < targetCards.length; ++m) {
              const tmp = [targetCards[i], targetCards[j], targetCards[k], targetCards[l], targetCards[m]]
              const tmpCardType = TexasCardUtil.getCardType(tmp)
              if (tmpCardType > cardType) {
                cardType = tmpCardType
                pickedCards = tmp
              }
            }
          }
        }
      }
    }

    return pickedCards
  }
}

export class AnalysePokeResult {
  fourCount = 0
  threeCount = 0
  twoCount = 0
  oneCount = 0
  fourLogicValue: number[] = []
  threeLogicValue: number[] = []
  twoLogicValue: number[] = []
  oneLogicValue: number[] = []

  static analyseCards = (cards: Poke[]): AnalysePokeResult => {
    const result = new AnalysePokeResult()
    for (let i = 0; i < cards.length; ++i) {
      let sameCount = 1
      const cardValue = cards[i].getValue()
      for (let j = i + 1; j < cards.length; ++j) {
        if (cards[j].getValue() !== cardValue) {
          break
        }
        ++sameCount
      }
      switch (sameCount) {
        case 1:
          result.oneLogicValue.push(cardValue)
          result.oneCount++
          break
        case 2:
          result.twoLogicValue.push(cardValue)
          result.twoCount++
          break
        case 3:
          result.threeLogicValue.push(cardValue)
          result.threeCount++
          break
        case 4:
          result.fourLogicValue.push(cardValue)
          result.fourCount++
          break
        default:
      }
      i += sameCount - 1
    }
    return result
  }
}
