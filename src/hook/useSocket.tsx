import { setUserId } from '@/store/controlSlice'
import {
  setEventState,
  setPlayerActionData,
  setPlayerCards,
  setRoomData,
  setWinProb,
  setWinners,
} from '@/store/roomSlice'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import io, { Socket } from 'socket.io-client'

const useSocket = () => {
  const [socket, setSocket] = useState<Socket | null>(null)

  const dispatch = useDispatch()

  const connect = (url: string) => {
    if (url === '' || socket !== null) return

    const s = io(url)
    setSocket(s)

    // random six digits id
    const rnd = Math.floor(Math.random() * 1000000)

    // add leading zeros
    const randomId = rnd.toString().padStart(6, '0')
    // const randomId = '349794'

    dispatch(
      setUserId({
        userId: randomId,
      }),
    )

    s.on('connect', () => {
      console.log(`Connected to ${url} with id ${randomId}`)

      s.emit('User:Login', {
        userId: randomId,
      })

      s.on('User:Login', (data) => {
        console.log(data)
      })

      s.on('Poker:RoomBroadcast', (message) => {
        dispatch(
          setRoomData({
            roomData: message.room,
          }),
        )

        console.log(message.eventState)

        dispatch(
          setEventState({
            eventState: message.eventState,
          }),
        )
      })

      s.on('PokerGame:RequestAction', (message: any) => {
        dispatch(
          setPlayerActionData({
            isMyTurn: true,
            betMap: message.betMap,
            allowedActions: message.actions,
            actionEndTime: message.actionEndTime,
            minRaise: message.minRaise,
          }),
        )
      })

      s.on('PokerGame:Deal', (message: any) => {
        console.log('deal cards', message)
        dispatch(
          setPlayerCards({
            playerCards: message.cards,
          }),
        )
      })

      s.on('PokerGame:Settle', (m: any) => {
        console.log(m)
        dispatch(
          setWinners({
            winners: m.winners,
          }),
        )
      })

      s.on('PokerGame:Probability', (m: any) => {
        console.log(m)
        dispatch(
          setWinProb({
            winProb: m.probability,
          }),
        )
      })

      s.on('error', (error) => {
        console.log(error)
      })
    })

    return () => {
      s.disconnect()
    }
  }

  return {
    socket,
    connect,
  }
}

export default useSocket
