import { IRoomConfig } from './room'

export enum GameState {
  Reset = 'Reset', // 轉莊, check全體夠唔夠最少注數, 唔夠就當輸, update Gamer狀態
  PreFlop = 'PreFlop', // 玩家派兩張牌
  Flop = 'Flop', // 開3張公牌, 玩家行動
  Turn = 'Turn', // 開1張公牌, 玩家行動
  River = 'River', // 開1張公牌, 玩家行動
  Showdown = 'Showdown', // 玩家比牌, 贏家拿注數
}

export enum PlayerAction {
  Check = 'Check',
  Call = 'Call',
  Raise = 'Raise',
  AllIn = 'AllIn',
  Fold = 'Fold',
}

export enum PlayerState {
  Active = 'Active',
  Fold = 'Fold',
  AllIn = 'AllIn',
  Out = 'Out',
}

export interface IPokerGame {
  roomId: string
  roomConfig: IRoomConfig
  state: GameState
  players: IPokerPlayerPublicInfo[]
  publicCards: number[]
  pot: number
  currActionPos: number
  actionEndTime: Date | null
  currBet: number
  lastRaisePos: number
}

export interface IPokerPlayerInfo {
  playerId: string
  chips: number
  seat: number
  position: number
  state: PlayerState
  currAction: PlayerAction | null
  lastAction: PlayerAction | null
  currentBet: number
  lastBet: number
}

export interface IPokerPlayerPrivateInfo extends IPokerPlayerInfo {
  cards: number[]
}

export interface IPokerPlayerPublicInfo extends IPokerPlayerInfo {
  numOfCards: number
}
