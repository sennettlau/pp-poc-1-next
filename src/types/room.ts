import { Action } from '@reduxjs/toolkit'
import { IPokerGame, IPokerPlayerPublicInfo } from './poker'

export interface IRoomState {
  roomId: string | null
  roomData: IPokerRoom | null
  eventState: PokerBroadcastEvent | null
  isMyTurn: boolean
  betMap: { [action: string]: number } // action: bet
  allowedActions: string[]
  actionEndTime: Date | null
  minRaise: number
  playerCards: number[]
  winners: string[]
  winProb: number
}

export interface IRoomSetRoomIdAction extends Action {
  payload: {
    roomId: string
  }
}

export interface IRoomSetRoomDataAction extends Action {
  payload: {
    roomData: IPokerRoom
  }
}

export interface IRoomSetEventStateAction extends Action {
  payload: {
    eventState: PokerBroadcastEvent
  }
}

export interface IRoomSetIsMyTurnAction extends Action {
  payload: {
    isMyTurn: boolean
  }
}

export interface IRoomSetPlayerActionDataAction extends Action {
  payload: {
    isMyTurn: boolean
    betMap: { [action: string]: number }
    allowedActions: string[]
    actionEndTime: Date
    minRaise: number
  }
}

export interface IRoomSetPlayerCardsAction extends Action {
  payload: {
    playerCards: number[]
  }
}

export interface IRoomSetWinnersAction extends Action {
  payload: {
    winners: string[]
  }
}

export interface IRoomSetWinProbAction extends Action {
  payload: {
    winProb: number
  }
}

export interface IRoomConfig {
  roomType: string
  chipValue: number //幾錢一注
  chipAmount: number //開局幾多注
  smallBlind: number //小盲注 要落幾多註
  bigBlind: number //大盲注 要落幾多註
  minChip: number //開新輪前如果注數不足, 踢走
}

export enum RoomState {
  Waiting = 'Waiting',
  Playing = 'Playing',
  Finished = 'Finished',
}

export interface IPokerRoom {
  roomId: string
  roomConfig: IRoomConfig
  state: RoomState
  players: IPokerPlayerPublicInfo[]
  game: IPokerGame | null
}

export enum PokerBroadcastEvent {
  StartGame = 'StartGame',
  Reset = 'Reset',
  ResetEnd = 'ResetEnd',
  PreFlop = 'PreFlop',
  PreFlopEnd = 'PreFlopEnd',
  Flop = 'Flop',
  FlopEnd = 'FlopEnd',
  Turn = 'Turn',
  TurnEnd = 'TurnEnd',
  River = 'River',
  RiverEnd = 'RiverEnd',
  Showdown = 'Showdown',
  PlayerActionStart = 'PlayerActionStart',
  PlayerActionEnd = 'PlayerActionEnd',
  EndGame = 'EndGame',
}
