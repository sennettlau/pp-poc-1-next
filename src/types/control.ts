import { Action } from 'redux'

export interface IControlState {
  userId: string | null
  isImageModalOpen: boolean
  imageModalSrc: string
}

export interface IControlSetImageModalOpenAction extends Action {
  payload: {
    isImageModalOpen: boolean
    imageModalSrc?: string
  }
}

export interface IControlSetUserIdAction extends Action {
  payload: {
    userId: string
  }
}
