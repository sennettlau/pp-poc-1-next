import { RootState } from '@/store'
import { setIsMyTurn } from '@/store/roomSlice'
import { IPokerRoom, PlayerAction } from '@/types'
import { Button, Flex, Text } from '@chakra-ui/react'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Socket } from 'socket.io-client'

type Props = {
  roomData: IPokerRoom | null
  socket: Socket | null
}

const ActionsList = (props: Props) => {
  const { roomData, socket } = props

  const allowedActions = useSelector(
    (state: RootState) => state.roomSlice.allowedActions,
  )
  const betMap = useSelector((state: RootState) => state.roomSlice.betMap)
  const actionEndTime = useSelector(
    (state: RootState) => state.roomSlice.actionEndTime,
  )
  const isMyTurn = useSelector((state: RootState) => state.roomSlice.isMyTurn)

  const [remainingTime, setRemainingTime] = useState<number>(0)

  const dispatch = useDispatch()

  const clickAction = (action: PlayerAction) => {
    if (!socket) return

    socket.emit('Poker:Action', {
      action,
      amount: betMap[action],
    })

    socket.on('Poker:Action', () => {
      console.log('Poker:Action success')
      dispatch(
        setIsMyTurn({
          isMyTurn: false,
        }),
      )
    })
  }

  useEffect(() => {
    if (actionEndTime) {
      const interval = setInterval(() => {
        const now = new Date().getTime()
        const endTime = new Date(actionEndTime)
        let distance = endTime.getTime() - now
        distance = Math.floor(distance / 1000)

        if (distance <= 0) {
          dispatch(
            setIsMyTurn({
              isMyTurn: false,
            }),
          )
        }

        setRemainingTime(distance)
      }, 1000)

      return () => clearInterval(interval)
    }
  }, [actionEndTime])

  return (
    <Flex w={'fit-content'} px={4} flexDir={'column'} gap={6}>
      <Text>
        {actionEndTime && isMyTurn
          ? `Time Remaining: ${remainingTime}`
          : 'Pending'}
      </Text>
      <Button
        colorScheme={
          allowedActions?.includes(PlayerAction.Check) && isMyTurn
            ? 'blue'
            : 'gray'
        }
        onClick={() => {
          clickAction(PlayerAction.Check)
        }}
        w={'200px'}
      >
        Check
      </Button>

      <Button
        colorScheme={
          allowedActions?.includes(PlayerAction.Call) && isMyTurn
            ? 'blue'
            : 'gray'
        }
        onClick={() => {
          clickAction(PlayerAction.Call)
        }}
        w={'200px'}
      >
        Call
      </Button>

      <Button
        colorScheme={
          allowedActions?.includes(PlayerAction.Raise) && isMyTurn
            ? 'blue'
            : 'gray'
        }
        onClick={() => {
          clickAction(PlayerAction.Raise)
        }}
        w={'200px'}
      >
        Raise
      </Button>

      <Button
        colorScheme={
          allowedActions?.includes(PlayerAction.AllIn) && isMyTurn
            ? 'blue'
            : 'gray'
        }
        onClick={() => {
          clickAction(PlayerAction.AllIn)
        }}
        w={'200px'}
      >
        All In
      </Button>

      <Button
        colorScheme={
          allowedActions?.includes(PlayerAction.Fold) && isMyTurn
            ? 'blue'
            : 'gray'
        }
        onClick={() => {
          clickAction(PlayerAction.Fold)
        }}
        w={'200px'}
      >
        Fold
      </Button>
    </Flex>
  )
}

export default ActionsList
