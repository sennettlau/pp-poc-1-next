import { RootState } from '@/store'
import { IPokerRoom, RoomState } from '@/types'
import { Flex, Text } from '@chakra-ui/react'
import { useSelector } from 'react-redux'

type Props = {
  roomData: IPokerRoom | null
}

const RoomPlayerList = (props: Props) => {
  const { roomData } = props
  const winners = useSelector((state: RootState) => state.roomSlice.winners)

  const getBG = (playerId: string, pos: number) => {
    if (winners.includes(playerId)) return 'yellow.100'
    else if (winners.length > 0) return 'gray.100'
    return roomData?.game?.currActionPos === pos ? 'blanc.100' : 'gray.100'
  }

  return (
    <>
      {roomData && (
        <Flex px={3} gap={4} flexWrap={'wrap'} flex={1}>
          {roomData.players.map((player) => (
            <Flex
              key={player.playerId}
              px={3}
              py={1}
              bg={getBG(player.playerId!, player.position!)}
              borderRadius={5}
              justifyContent={'space-between'}
              w={'200px'}
              flexDir={'column'}
              h={'fit-content'}
            >
              <Text>
                Player: {player.playerId}{' '}
                {player.position === 0
                  ? '(D)'
                  : player.position === 1
                  ? '(SB)'
                  : player.position === 2
                  ? '(BB)'
                  : ''}
              </Text>
              <Text>Chip: {player.chips}</Text>
              {roomData.state === RoomState.Playing && (
                <>
                  <Text>Cards: {player.numOfCards || 0}</Text>
                  <Flex justifyContent={'space-between'}>
                    <Text>Bet: {player.currentBet}</Text>
                    <Text> {player.lastAction}</Text>
                  </Flex>
                </>
              )}
            </Flex>
          ))}
        </Flex>
      )}
    </>
  )
}

export default RoomPlayerList
