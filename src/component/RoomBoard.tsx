import { RootState } from '@/store'
import { Flex } from '@chakra-ui/react'
import { useSelector } from 'react-redux'
import { Socket } from 'socket.io-client'
import ActionsList from './ActionList'
import CardList from './CardList'
import RoomBoardTop from './RoomBoardTop'
import RoomPlayerList from './RoomPlayerList'

type Props = {
  socket: Socket | null
}

const RoomBoard = (props: Props) => {
  const { socket } = props
  const roomData = useSelector((state: RootState) => state.roomSlice.roomData)

  return (
    <>
      {roomData && (
        <Flex
          flex={1}
          flexDir={'column'}
          gap={12}
          w={'100%'}
          maxW={'1320px'}
          pt={4}
        >
          <RoomBoardTop roomData={roomData} socket={socket} />
          <CardList roomData={roomData} />
          <Flex>
            <RoomPlayerList roomData={roomData} />
            <ActionsList roomData={roomData} socket={socket} />
          </Flex>
        </Flex>
      )}
    </>
  )
}

export default RoomBoard
