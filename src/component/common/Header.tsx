import { RootState } from '@/store'
import { Flex, Text } from '@chakra-ui/react'
import { useEffect } from 'react'
import { useSelector } from 'react-redux'

const Header = () => {
  const userId = useSelector((state: RootState) => state.controlSlice.userId)

  useEffect(() => {
    console.log(userId)
  }, [userId])

  return (
    <Flex
      height={70}
      alignItems={'center'}
      px={3}
      shadow={'md'}
      width={'100%'}
      position={'fixed'}
      zIndex={100}
      justifyContent={'space-between'}
    >
      <Text fontWeight={'bold'} color={'blanc.200'} fontSize={'xl'}>
        PP POC NEXT APP
      </Text>
      <Flex alignItems={'center'}>{userId}</Flex>
    </Flex>
  )
}

export default Header
