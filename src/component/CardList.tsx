import { RootState } from '@/store'
import { IPokerRoom } from '@/types'
import { Poke } from '@/utils/poker-core'
import { Flex, Text } from '@chakra-ui/react'
import { useSelector } from 'react-redux'

type Props = {
  roomData: IPokerRoom | null
}

const CardList = (props: Props) => {
  const { roomData } = props

  const cards = useSelector((state: RootState) => state.roomSlice.playerCards)
  const winProb = useSelector((state: RootState) => state.roomSlice.winProb)

  return (
    <Flex gap={4} p={4} w={'100%'} h={'150px'}>
      <Flex w={'60%'} gap={4} pr={4}>
        Public Cards:
        {roomData?.game?.publicCards.map((card) => (
          <Flex
            key={card}
            bg={'gray.200'}
            borderRadius={5}
            w={'100px'}
            h={'100%'}
            justifyContent={'center'}
            alignItems={'center'}
          >
            {Poke.FromByte(card).toString()}
          </Flex>
        ))}
      </Flex>
      <Flex w={'30%'} gap={4}>
        Player Cards:
        {cards.map((card) => (
          <Flex
            key={card}
            bg={'gray.200'}
            borderRadius={5}
            w={'100px'}
            h={'100%'}
            justifyContent={'center'}
            alignItems={'center'}
          >
            {Poke.FromByte(card).toString()}
          </Flex>
        ))}
      </Flex>
      <Flex w={'130px'} flexDir={'column'} gap={2}>
        <Text>Winning Prob:</Text>
        <Text fontSize={'2xl'}>{(winProb * 100).toFixed(5)}%</Text>
      </Flex>
    </Flex>
  )
}

export default CardList
