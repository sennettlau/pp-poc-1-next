import useSocket from '@/hook/useSocket'
import { RootState } from '@/store'
import { setRoomData, setRoomId } from '@/store/roomSlice'
import { Button, Flex } from '@chakra-ui/react'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Socket } from 'socket.io-client'
import RoomBoard from './RoomBoard'

const MainBoard = () => {
  const roomId = useSelector((state: RootState) => state.roomSlice.roomId)

  const [s, setS] = useState<Socket | null>(null)
  const { socket, connect } = useSocket()

  const dispatch = useDispatch()

  const connectToNonProd = () => {
    connect('https://rp-cn-non-prod-poc1.kokmm.net')
  }

  const connectToLocal = () => {
    connect('http://localhost:3000')
  }

  const joinRoom = () => {
    if (!socket) return

    setS(socket)

    socket.emit('Poker:Join', {})

    socket.on('Poker:Join', (message) => {
      let data = JSON.parse(message).data

      console.log(data)
      dispatch(
        setRoomId({
          roomId: data.roomId,
        }),
      )
      dispatch(
        setRoomData({
          roomData: data,
        }),
      )
    })
  }

  return (
    <Flex
      flex={1}
      flexDir={'column'}
      w={'100%'}
      justifyContent={'center'}
      align={'center'}
    >
      {socket ? (
        <>
          {roomId ? (
            <RoomBoard socket={s}/>
          ) : (
            <Button ml={3} px={6} colorScheme={'blue'} onClick={joinRoom}>
              Join Room
            </Button>
          )}
        </>
      ) : (
        <Flex
          flex={1}
          w={'100%'}
          align={'center'}
          flexDir={'column'}
          justifyContent={'center'}
          gap={4}
        >
          <Button colorScheme={'blue'} onClick={connectToLocal} w={'200px'}>
            Local
          </Button>
          <Button colorScheme={'blue'} onClick={connectToNonProd} w={'200px'}>
            Non Prod
          </Button>
        </Flex>
      )}
    </Flex>
  )
}

export default MainBoard
