import { RootState } from '@/store'
import { setEventState, setRoomData } from '@/store/roomSlice'
import { IPokerRoom } from '@/types'
import { Button, Flex, Text } from '@chakra-ui/react'
import { useDispatch, useSelector } from 'react-redux'
import { Socket } from 'socket.io-client'

type Props = {
  roomData: IPokerRoom | null
  socket: Socket | null
}

const RoomBoardTop = (props: Props) => {
  const { roomData, socket } = props

  const userId = useSelector((state: RootState) => state.controlSlice.userId)
  const eventState = useSelector(
    (state: RootState) => state.roomSlice.eventState,
  )

  const dispatch = useDispatch()

  const startGame = () => {
    console.log(`start game ${!!socket}`)
    if (!socket) return

    socket.emit('Poker:StartGame', {})

    socket.on('Poker:StartGame', (message) => {})
  }

  return (
    <>
      {roomData && (
        <Flex>
          <Flex flexDir={'column'} gap={3} flex={1}>
            <Flex gap={8} px={3} pt={3}>
              <Text>Room: {roomData.roomId}</Text>
              <Text>Player: {roomData.players.length}</Text>
            </Flex>
            <Flex gap={6} px={3}>
              <Text>Type: {roomData.roomConfig.roomType}</Text>
              <Text>Chip Value: {roomData.roomConfig.chipValue}</Text>
              <Text>Chip Amount: {roomData.roomConfig.chipAmount}</Text>
              <Text>Small Blind: {roomData.roomConfig.smallBlind}</Text>
              <Text>Big Blind: {roomData.roomConfig.bigBlind}</Text>
              <Text>Min Chip: {roomData.roomConfig.minChip}</Text>
            </Flex>
          </Flex>
          <Flex w={'200px'} align={'center'} px={3}>
            {eventState ? (
              <Text>{eventState}</Text>
            ) : (
              <Button
                colorScheme={
                  roomData.players.length < 2 ||
                  roomData.players[0].playerId! !== userId
                    ? 'gray'
                    : 'blue'
                }
                w={'100%'}
                onClick={startGame}
              >
                Start Game
              </Button>
            )}
          </Flex>
        </Flex>
      )}
    </>
  )
}

export default RoomBoardTop
