import {
  IRoomSetEventStateAction,
  IRoomSetIsMyTurnAction,
  IRoomSetPlayerActionDataAction,
  IRoomSetRoomDataAction,
  IRoomSetRoomIdAction,
  IRoomState,
} from '@/types'
import { createSlice } from '@reduxjs/toolkit'
import {
  IRoomSetPlayerCardsAction,
  IRoomSetWinProbAction,
  IRoomSetWinnersAction,
} from '../types/room'

const initialState: IRoomState = {
  roomId: null,
  roomData: null,
  eventState: null,
  isMyTurn: false,
  betMap: {},
  allowedActions: [],
  actionEndTime: null,
  minRaise: 0,
  playerCards: [],
  winners: [],
  winProb: 0,
}

const slice = createSlice({
  name: 'control',
  initialState,
  reducers: {
    setRoomId(state, action: IRoomSetRoomIdAction) {
      state.roomId = action.payload.roomId
    },
    setRoomData(state, action: IRoomSetRoomDataAction) {
      state.roomData = action.payload.roomData
    },
    setEventState(state, action: IRoomSetEventStateAction) {
      state.eventState = action.payload.eventState
    },
    setIsMyTurn(state, action: IRoomSetIsMyTurnAction) {
      state.isMyTurn = action.payload.isMyTurn
    },
    setPlayerActionData(state, action: IRoomSetPlayerActionDataAction) {
      state.isMyTurn = action.payload.isMyTurn
      state.betMap = action.payload.betMap
      state.allowedActions = action.payload.allowedActions
      state.actionEndTime = action.payload.actionEndTime
      state.minRaise = action.payload.minRaise
    },
    setPlayerCards(state, action: IRoomSetPlayerCardsAction) {
      state.playerCards = action.payload.playerCards
    },
    setWinners(state, action: IRoomSetWinnersAction) {
      state.winners = action.payload.winners
    },
    setWinProb(state, action: IRoomSetWinProbAction) {
      state.winProb = action.payload.winProb
    },
  },
})

export const {
  setRoomId,
  setRoomData,
  setEventState,
  setIsMyTurn,
  setPlayerActionData,
  setPlayerCards,
  setWinners,
  setWinProb
} = slice.actions

export default slice
