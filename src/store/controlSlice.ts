import {
  IControlSetImageModalOpenAction,
  IControlSetUserIdAction,
  IControlState,
} from '@/types'
import { createSlice } from '@reduxjs/toolkit'

const initialState: IControlState = {
  userId: null,
  isImageModalOpen: false,
  imageModalSrc: '',
}

const slice = createSlice({
  name: 'control',
  initialState,
  reducers: {
    setImageModalOpen(state, action: IControlSetImageModalOpenAction) {
      state.isImageModalOpen = action.payload.isImageModalOpen
      state.imageModalSrc = action.payload.imageModalSrc || ''
    },
    
    setUserId(state, action: IControlSetUserIdAction) {
      state.userId = action.payload.userId
    },
  },
})

export const {
  setImageModalOpen,
  setUserId
} = slice.actions

export default slice
